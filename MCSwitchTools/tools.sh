#!/bin/bash
# Written by Luke Chambers
# Licensed under GPLv3

export MCST_VERSION=1.1

mc () {
	if [ "$1" = help ]; then
		echo "MCSwitchTools v$MCST_VERSION"
		echo "List of subcommands:"
		echo
		echo "  help - Prints this help menu"
		echo "  run - Runs the Minecraft launcher"
		echo "  lwjgl <2|3> - Changes the LWJGL version"
		echo "  init - Sets Minecraft up for the first time"
		echo "  profiles - Fixes all game profiles to prevent crashes"
		echo
		echo "If you need help, feel free to contact Luke#3416 on Discord"
		echo "You can also contact @ConsoleLogLuke on Twitter if you don't use Discord"
	elif [ "$1" = run ]; then
		export _JAVA_OPTIONS="-Djava.library.path=$HOME/.minecraft/natives/"
		if [ -f ~/.minecraft/natives/libglfw.so ]; then
			echo "MCSwitchTools - Starting Minecraft launcher..."
			echo "  Java version:       OpenJDK 11"
			echo "  LWJGL version:      LWJGL 3"
			echo "  Minecraft versions: 1.13+"
			echo
			/usr/lib/jvm/java-11-openjdk-arm64/bin/java -jar ~/MCSwitchTools/launcher.jar
		else
			echo "MCSwitchTools - Starting Minecraft launcher..."
			echo "  Java version:       OpenJDK 8"
			echo "  LWJGL version:      LWJGL 2"
			echo "  Minecraft versions: 1.12.2 and below"
			echo
			/usr/lib/jvm/java-8-openjdk-arm64/jre/bin/java -jar ~/MCSwitchTools/launcher.jar
		fi
		unset _JAVA_OPTIONS
	elif [ "$1" = lwjgl ]; then
		if [ "$2" = 2 ]; then
			rm -rf ~/.minecraft/natives/*
			cp ~/MCSwitchTools/natives/2/* ~/.minecraft/natives
		elif [ "$2" = 3 ]; then
			rm -rf ~/.minecraft/natives/*
			cp ~/MCSwitchTools/natives/3/* ~/.minecraft/natives
		else
			echo "Invalid LWJGL version; please choose '2' or '3'"
		fi
	elif [ "$1" = init ]; then
		if grep -q ". ~/MCSwitchTools/tools.sh" ~/.bashrc; then
			echo ".bashrc seems to already import MCSwitchTools functions; skipping"
		else
			echo >> ~/.bashrc
			echo "# Import MCSwitchTools functions" >> ~/.bashrc
			echo ". ~/MCSwitchTools/tools.sh" >> ~/.bashrc
		fi
		sudo apt install openjdk-11-jdk openjdk-8-jdk
		mkdir -p ~/.minecraft/natives
		chmod +x ~/MCSwitchTools/profile_fix.py
		echo
		echo "You're all prepared to run Minecraft!"
		echo "You'll need to restart your terminal to complete the setup"
		echo "Please refer back to the guide you're following to do these steps:"
		echo "  Set your LWJGL version"
		echo "  Run the Minecraft launcher"
		echo "  Fix your launcher profiles"
	elif [ "$1" = profiles ]; then
		python3 ~/MCSwitchTools/profile_fix.py
	else
		echo "Invalid subcommand; run 'mc help' for a list of subcommands"
	fi
}

if [ $# -ne 0 ]; then
	mc $@
fi
