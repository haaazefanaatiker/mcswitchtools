# MCSwitchTools

Small toolkit for easily managing Minecraft: Java Edition on L4T Ubuntu

## Known Issues

* If using Forge, the game will crash after loading mods

## Installation

### Step 0: Prerequisites

* L4T Ubuntu

That's it, all the other prerequisites are automatically installed for you when you install MCSwitchTools for the first time.

### Step 1: Downloading MCSwitchTools

Download the latest version of MCSwitchTools from [the release folder](https://gitlab.com/devluke/mcswitchtools/tree/master/release).

Extract the file you downloaded and make sure the name of the extracted folder is `MCSwitchTools`. If it is not named that, rename it.

Move this folder to your home folder.

### Step 2: Installing MCSwitchTools

Open Terminal from either the launcher or by using the key combination `Control+Alt+T`.

Run these commands inside Terminal; enter your password if it prompts you for it:

`sudo apt update`

`~/MCSwitchTools/tools.sh init`

After running those commands, restart Terminal.

### Step 3: Selecting your LWJGL version

Now, you need to choose a LWJGL version. The options are `2` or `3`. If you plan on using Minecraft 1.13+, choose `3`. If you plan on using Minecraft 1.12.2 or below, choose `2`. You can change this at any time.

To choose your LWJGL version, simply run this command inside Terminal:

`mc lwjgl (2 or 3)`

If you're not sure which version to choose, just choose `3`:

`mc lwjgl 3`

### Step 4: Running the launcher and fixing your profiles

It's finally time to start the Minecraft launcher. Run this command to run the launcher:

`mc run`

Once the launcher opens, log in to your Mojang or Minecraft account then close the launcher.

For Minecraft to actually work, you'll need to run this command:

`mc profiles`

Any time you add a new launcher profile (for OptiFine, Forge, etc.), you'll need to run that command to prevent Minecraft from crashing.

### Step 5: Starting Minecraft

Any time you want to play Minecraft, you'll have to run this command in Terminal:

`mc run`

After all the previous steps have been completed, you can run that command and play Minecraft as you normally would. Enjoy!

It is highly recommended that you check out the 'Tips' section below to get better FPS when playing Minecraft, though it is not necessary.

## Tips

* Create a swapfile. This will help you gain more memory and, since the Switch only has 4GB, this will help out a lot. Without this, your whole system could freeze while playing because all the memory is taken up, requiring a restart. [Here's an easy guide](https://gbatemp.net/threads/l4t-ubuntu-applcation-install-guides.537579/) to create a swapfile on L4T Ubuntu.

* Use [OptiFine](https://optifine.net/home). This may be obvious but it really does help with FPS. OptiFine can be installed the same way you normally install it on other platforms.

* Run `sudo jetson_clocks` before opening Minecraft. This will make your whole system perform slightly better, and it does make a small impact on Minecraft. You'll need to run this command each time you reboot.

## Changelog

### Version 1.0

* Initial release

### Version 1.1

* Fixed support for Minecraft 1.12.2 and below
* Added an info message when starting the launcher
* Removed an unneeded message from the end of the `init` subcommand
